variable "helm_charts" {
  type = map(object({
    name             = string
    repository       = string
    chart            = string
    tags             = map(string)
    namespace        = string
    create_namespace = bool
    values           = string
  }))
  default = {
    "argocd" = {
      name       = "argocd"
      repository = "https://argoproj.github.io/argo-helm"
      chart      = "argo-cd"
      tags = {
        "owner"    = "vcloud-lab.com"
        "downtime" = "afternoon"
      }
      namespace        = "argocd"
      create_namespace = true
      values           = "helm-values/argocd.yaml"
    }
  }
}

