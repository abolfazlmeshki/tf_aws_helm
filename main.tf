resource "helm_release" "helm_charts" {
  for_each         = var.helm_charts
  name             = each.value["name"]
  repository       = each.value["repository"]
  chart            = each.value["chart"]
  namespace        = each.value["namespace"]
  create_namespace = each.value["create_namespace"]
  values = [
    file(each.value["values"])
  ]
}