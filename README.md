<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.8.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.8.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.helm_charts](https://registry.terraform.io/providers/hashicorp/helm/2.8.0/docs/resources/release) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_helm_charts"></a> [helm\_charts](#input\_helm\_charts) | n/a | <pre>map(object({<br>    name             = string<br>    repository       = string<br>    chart            = string<br>    tags             = map(string)<br>    namespace        = string<br>    create_namespace = bool<br>    values           = string<br>  }))</pre> | <pre>{<br>  "argocd": {<br>    "chart": "argo-cd",<br>    "create_namespace": true,<br>    "name": "argocd",<br>    "namespace": "argocd",<br>    "repository": "https://argoproj.github.io/argo-helm",<br>    "tags": {<br>      "downtime": "afternoon",<br>      "owner": "vcloud-lab.com"<br>    },<br>    "values": "helm-values/argocd.yaml"<br>  }<br>}</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->